import 'package:flutter/material.dart';
import 'package:riego_tech/routes.dart';
import 'package:riego_tech/screens/home/home_screen.dart';
import 'package:riego_tech/screens/sign_in/sign_in_screen.dart';
import 'package:riego_tech/screens/splash/splash_screen.dart';
import 'package:riego_tech/theme.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:riego_tech/auth_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_) => AuthService(FirebaseAuth.instance),
        ),
        StreamProvider(
          create: (context) => context.read<AuthService>().authStateChanges,
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'RiegoTech',
        theme: theme(),
        // home: SplashScreen(),
        // We use routeName so that we dont need to remember the name
        initialRoute: SplashScreen.routeName,
        routes: routes,
      ),
    );

    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   title: 'RiegoTech',
    //   theme: theme(),
    //   // home: SplashScreen(),
    //   // We use routeName so that we dont need to remember the name
    //   initialRoute: SplashScreen.routeName,
    //   routes: routes,
    // );
  }
}

class AuthenticationWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User>();

    if (firebaseUser != null) {
      return HomeScreen();
    }
    return SignInScreen();
  }
}
