import 'package:firebase_auth/firebase_auth.dart';
import 'package:twitter_login/schemes/auth_result.dart';
// import 'package:flutter_twitter_login/flutter_twitter_login.dart';
// import 'package:flutter_twitter/flutter_twitter.dart';
import 'package:twitter_login/twitter_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

final String TWITTER_API = "e46mRZtrTueS8L1vme8vyT2Q6";
final String TWITTER_SECRET =
    "hqFgPX2lU8GYlYSr7gNrYuPWpcgrg1fDJzQ571yuthGOL2BMV0";

class AuthService {
  final FirebaseAuth _firebaseAuth;
  // GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  // GoogleSignIn _googleSignIn = GoogleSignIn();

  //                    AQUI EMPIEZA LOGIN TWITTER

  void logTwit() async {
    final twitterLogin = new TwitterLogin(
      // Consumer API keys
      apiKey: 'e46mRZtrTueS8L1vme8vyT2Q6',
      apiSecretKey: 'hqFgPX2lU8GYlYSr7gNrYuPWpcgrg1fDJzQ571yuthGOL2BMV0',
      // Callback URL for Twitter App
      // Android is a deeplink
      // iOS is a URLScheme
      redirectURI:
          'https://pruebaregistro-aae4b.firebaseapp.com/__/auth/handler',
    );
    // If you want to implement Twitter account switching, set [force_login] to true
    // login(forceLogin: true);
    final authResult = await twitterLogin.login();
    switch (authResult.status) {
      case TwitterLoginStatus.loggedIn:
        // success
        break;
      case TwitterLoginStatus.cancelledByUser:
        // cancel
        break;
      case TwitterLoginStatus.error:
        // error
        break;
    }
  }

  // Future<UserCredential> loginTwitter() async {
  //   final TwitterLogin twitterLogin = new TwitterLogin(
  //       consumerKey: TWITTER_API, consumerSecret: TWITTER_SECRET);

  //   final TwitterLoginResult loginResult = await twitterLogin.authorize();

  //   final TwitterSession twitterSession = loginResult.session;

  //   final AuthCredential twitterCredential = TwitterAuthProvider.credential(
  //       accessToken: twitterSession.token, secret: twitterSession.secret);

  //   return await FirebaseAuth.instance.signInWithCredential(twitterCredential);
  // }

  // void twitterLog() async {
  //   var twitterLogin = new TwitterLogin(
  //     consumerKey: 'e46mRZtrTueS8L1vme8vyT2Q6',
  //     consumerSecret: 'hqFgPX2lU8GYlYSr7gNrYuPWpcgrg1fDJzQ571yuthGOL2BMV0',
  //   );

  //   final TwitterLoginResult result = await twitterLogin.authorize();

  //   switch (result.status) {
  //     case TwitterLoginStatus.loggedIn:
  //       var session = result.session;
  //       print(session.username);
  //       // _sendTokenAndSecretToServer(session.token, session.secret);
  //       break;
  //     case TwitterLoginStatus.cancelledByUser:
  //       // _showCancelMessage();
  //       print('Cancelado');
  //       break;
  //     case TwitterLoginStatus.error:
  //       print(result.errorMessage);
  //       // _showErrorMessage(result.error);
  //       break;
  //   }
  // }

  //          AQUI TERMINA LOGIN TWITTER

  Future<UserCredential> loginGoogle() async {
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  // Future gooSignIn() async {
  //   try {
  //     await _googleSignIn.signIn();
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  AuthService(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.idTokenChanges();

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  Future<String> signIn({String email, String password}) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return "Ingresado";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  Future<String> signUp({String email, String password}) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);
      return "Registrado";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }
}
